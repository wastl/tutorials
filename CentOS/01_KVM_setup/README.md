setup a host machine with CentOS7

ssh into your fresh host and tunnel the vnc port:
```bash
ssh USER@HOST -L 127.0.0.1:5900:127.0.0.1:5900
```

to make the network interface static, edit in /etc/sysconfig/network-scripts/ifcfg-{enp2s0}(interfacename {enp2s0} can be different):
```bash
BOOTPROTO="static"
DEFROUTE="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="no"
IPADDR="192.168.178.36"
GATEWAY="192.168.178.1"
DNS1="192.168.178.1"
DOMAIN="192.168.178.1"
```

```bash
yum install kvm virt-manager libvirt virt-install qemu policycoreutils-python @virt* dejavu-lgc-* xorg-x11-xauth tigervnc libguestfs-tools policycoreutils-python bridge-utils
```

expecting storing VM's in /vm:
```bash
mkdir /vm
semanage fcontext -a -t virt_image_t "/vm(/.*)?"; restorecon -R /vm 
```

Allow packet forwarding between interfaces:
```bash
sed -i 's/^\(net.ipv4.ip_forward =\).*/\1 1/' /etc/sysctl.conf; sysctl -p
```

Configure libvirtd service to start automatically and reboot.
```bash
chkconfig libvirtd on; shutdown -r now
```

setting up a network bridg:
```bash
chkconfig network on
service network restart
yum -y erase NetworkManager
cp -p /etc/sysconfig/network-scripts/ifcfg-{enp2s0,br0}
sed -i -e'/HWADDR/d' -e'/UUID/d' -e's/enp2s0/br0/' -e's/Ethernet/Bridge/' \
> /etc/sysconfig/network-scripts/ifcfg-br0
echo DELAY=0 >> /etc/sysconfig/network-scripts/ifcfg-br0
echo 'BOOTPROTO="none"' >> /etc/sysconfig/network-scripts/ifcfg-enp2s0
echo BRIDGE=br0 >> /etc/sysconfig/network-scripts/ifcfg-enp2s0
service network restart
brctl show
```

```bash
mkdir /vm/NAME
cd /vm/NAME
```
create the qcow2 file, and download an iso:
```bash
qemu-img create -f qcow2 ./NAME.qcow2 10G
wget http://desired/OS/version.iso
```

create a file for the multiline command, to edit it compfortably. 
```bash
nano deploy.sh
```
```bash
virt-install \
 --connect qemu:///system \
 --name NAME \
 --ram 1024 \
 --disk path=./00_master.qcow2,size=10 \
 --vcpus 1 \
 --accelerate \
 --os-type linux \
 --os-variant centos7.0 \
 --network bridge=br0 \
 --graphics vnc,listen=0.0.0.0 \
 --noautoconsole \
 --hvm \
 --console pty,target_type=serial \
 --cdrom ./centos7.iso 
```
and make it executable and execute it:
```bash
chmod +x deploy.sh
./deploy.sh
```

now its time to connect via VNC to the vm to complete the installation.
Due to the port tunnel we can connect to ```localhost:5900```

