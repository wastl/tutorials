+ Outbound open ports 80, 443
+ Inbound open ports 80, 443, 5222 (only if you want to push actions to client machines) and 5269 (only for push actions to a Spacewalk Proxy), 69 udp if you want to use tftp

setup repos:
```bash
rpm -Uvh http://yum.spacewalkproject.org/2.7/RHEL/7/x86_64/spacewalk-repo-2.7-2.el7.noarch.rpm
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
(cd /etc/yum.repos.d && curl -O https://copr.fedorainfracloud.org/coprs/g/spacewalkproject/java-packages/repo/epel-7/group_spacewalkproject-java-packages-epel-7.repo)
```

setup database (vm with repos above):
```bash
yum install postgresql-server postgresql-contrib
postgresql-setup initdb
systemctl start postgresql
systemctl enable postgresql
su - postgres -c 'PGPASSWORD=spacepw; createdb -E UTF8 spaceschema ; createlang plpgsql spaceschema ; createlang pltclu spaceschema ; yes $PGPASSWORD | createuser -P -sDR spaceuser'
```

in /var/lib/pgsql/data/pg_hba.conf add before **all**
```bash
local spaceschema spaceuser md5
host  spaceschema spaceuser 127.0.0.1/8 md5
host  spaceschema spaceuser ::1/128 md5
local spaceschema postgres  ident
```

in /var/lib/pgsql/data/postgresql.conf edit:
```
listen_addresses = '*'
```

install the package:
```bash
yum install spacewalk-setup-postgresql
```

and run the setup(follow steps inside):
```bash
DBNAME=spaceschema
DBUSER=spaceuser
DBPASSWORD=pacepw
spacewalk-setup-postgresql create \
 --db $DBNAME \
 --user $DBUSER \
 --password $DBPASSWORD \
 --standalone
```

open Port 5432/tcp

now we can tune postgresql:
```bash
yum install pgtune
pgtune --type=web -c 600 -i /var/lib/pgsql/data/postgresql.conf >/tmp/pgtune.conf
# Review the changes by
diff -u /var/lib/pgsql/data/postgresql.conf /tmp/pgtune.conf
mv /var/lib/pgsql/data/postgresql.conf{,.bak}
cp /tmp/pgtune.conf /var/lib/pgsql/data/postgresql.conf
service postgresql restart
```

back on the spacewalk server install spacewalk:
```bash
yum -y install spacewalk-postgresql 
spacewalk-setup --external-postgresql
```
and follow the instruction

on clients install the client-repo and packages:
```bash
rpm -Uvh http://yum.spacewalkproject.org/2.7-client/RHEL/7/x86_64/spacewalk-client-repo-2.7-2.el7.noarch.rpm
rpm -Uvh http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum -y install rhn-client-tools rhn-check rhn-setup rhnsd m2crypto yum-rhn-plugin
# and additionally on the virtualization host
yum -y install rhn-virtualization-host
```

download the certificate from your spacewalk server:
```bash
rpm -Uvh http://YourSpacewalk.example.com/pub/rhn-org-trusted-ssl-cert-1.0-1.noarch.rpm
```
 register your system (you have to create a key before):
 ```bash
rhnreg_ks --serverUrl=https://YourSpacewalk.example.org/XMLRPC --sslCACert=/usr/share/rhn/RHN-ORG-TRUSTED-SSL-CERT --activationkey=<key-with-rhel-custom-channel>
```