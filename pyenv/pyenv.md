First, we need to install some dependencies
```bash
sudo apt-get install  make curl git build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev
```

then we can download the installscript and execute it

```bash
curl -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash
```

for __pyenv__ to work propper, we have to add three lines to ```~/.bashrc``` and reload the file, so execute

```bash
echo 'export PATH="~/.pyenv/bin:$PATH"' | tee -a ~/.bashrc
echo 'eval "$(pyenv init -)"'  | tee -a ~/.bashrc
echo 'eval "$(pyenv virtualenv-init -)"' | tee -a ~/.bashrc
source ~/.bashrc
```

after that we can install our desired ```Python```version (I'm taking 3.6.4 here)
```bash
pyenv install 3.6.4
```

making a virtualenvironment with the
__name__ ```name_me_better``` and making it __local__ (keep in mind, you have to be in the projects __root__folder)
```bash
pyenv virtualenv 3.6.4 name_me_better
pyenv local name_me_better
```
with this systanx you'll have a virtualenv, which will be automatically activated by entering the projects root folder.

if you'd like using the regular virtualenv
```bash
~/.pyenv/versions/3.6.4/bin/pip install virtualenv
cd /to/your/project
~/.pyenv/versions/3.6.4/bin/virtualenv venv
```
now you have the regual virtualenv, pointing to the version in ~/.pyenv/versions/

to activate it, do it as usual, from projects root folder
```bash
source venv/bin/activate
```

